﻿using System;
using System.Collections.Generic;
using System.Text;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;
using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using SourceCode.SmartObjects.Client;
//using SourceCode.Data.SmartObjectsClient;
using SourceCode.Hosting.Client;
using System.Linq;
using System.Collections;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.xml;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;

using System.Text.RegularExpressions;

namespace PlacerCounty.DocMerge
{
    [SourceCode.SmartObjects.Services.ServiceSDK.Attributes.ServiceObject(
    "ServiceObject", "ServiceObject", "Merges SQL Data into template and converts to PDF")]
    class ServiceObject
    {
        public ServiceObject()
        {
        }
        public ServiceConfiguration ServiceConfiguration { get; set; }
        private int _EmployeeID;
        [SourceCode.SmartObjects.Services.ServiceSDK.Attributes.Property(
           "EmployeeID",
            SoType.Text,
           "EmployeeID",
           "EmployeeID")]
        public int EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }

        private int _seqNo;
        [SourceCode.SmartObjects.Services.ServiceSDK.Attributes.Property(
           "SeqNo",
            SoType.Text,
           "SeqNo",
           "SeqNo")]
        public int SeqNo
        {
            get { return _seqNo; }
            set { _seqNo = value; }
        }
        private string _TemplateName;
        [SourceCode.SmartObjects.Services.ServiceSDK.Attributes.Property(
           "TemplateName",
            SoType.Text,
           "TemplateName",
           "Row Number")]
        public string TemplateName
        {
            get { return _TemplateName; }
            set { _TemplateName = value; }
        }
        private string _PDF;
        [SourceCode.SmartObjects.Services.ServiceSDK.Attributes.Property(
          "PDF",
           SoType.File,
          "PDF",
          "PDF File")]
        public string PDF
        {
            get { return _PDF; }
            set { _PDF = value; }
        }

        private string _EVALPDFName;
        [SourceCode.SmartObjects.Services.ServiceSDK.Attributes.Property(
           "EVALPDFName",
            SoType.Text,
           "EVALPDFName",
           "EVALPDFName")]
        public string EVALPDFName
        {
            get { return _EVALPDFName; }
            set { _EVALPDFName = value; }
        }
        [SourceCode.SmartObjects.Services.ServiceSDK.Attributes.Method(
            "CreateDocumentFromTemplate", SourceCode.SmartObjects.Services.ServiceSDK.Types.MethodType.Update, "CreateDocumentFromTemplate",
            "Create Document From Template",
            new string[] { "EmployeeID", "TemplateName", "EVALPDFName" },
            new string[] { "EmployeeID", "TemplateName", "EVALPDFName" },
            new string[] { "PDF" })]
        public ServiceObject CreateDocumentFromTemplate()
        {
            //Method implementation
            //Note:  You can implement the method here or inanother class file

            string strMapPath = string.Empty;
            if (!string.IsNullOrEmpty((string)ServiceConfiguration["HTMLDocumentPath"]))
            {
                strMapPath = (string)ServiceConfiguration["HTMLDocumentPath"];
            }
            string PDFName = string.Empty;
            DataSet ds = GetDocumentData(EmployeeID);
            //DataSet ds1 = GetImagePaths();
            string companyLogoPath = (string)ServiceConfiguration["ComplanyLogoPath"];

            PDFName = EVALPDFName;

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    //string strDocumentPath = getDocumentTemplate(strMapPath + "NewFolder1\\" + "CLIPPTemplate.docx", strMapPath);

                    //WordprocessingDocument doc = WordprocessingDocument.Open(strDocumentPath, true);
                    //Start 15-March-2013
                    string pdfPath = Convert.ToString(PDFName) + ".pdf";
                    //End 15-March-2013
                    try
                    {
                        string strDocumentPath = getDocumentTemplate(strMapPath + TemplateName, strMapPath);
                        StreamReader streamReader = new StreamReader(strDocumentPath);
                        string strHtml = streamReader.ReadToEnd();
                        streamReader.Close();
                        string performanceGoalstd = "<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'><td width=619 valign=top style='width:6.45in;padding:0in 5.4pt 0in 5.4pt'><p class=MsoNormal><b style='mso-bidi-font-weight:normal'><i style='mso-bidi-font-style:normal'><span style='font-family:'Calibri','sans-serif'; mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin'><BR/>Goal {$_performancegoal.No}: {$_performancegoal.Name}</span></i></b><b style='mso-bidi-font-weight:normal'><i style='mso-bidi-font-style:normal'><span style='font-family:'Calibri','sans-serif'; mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin'><BR />Achievement Level:</span></i></b><span style='font-family:'Calibri','sans-serif'; mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin'> {$_performancegoal.AchievementLevel}</span><b style='mso-bidi-font-weight:normal'><span  style='font-family:'Calibri','sans-serif';mso-ascii-theme-font:major-latin;  mso-hansi-theme-font:major-latin'><BR />Comments</span></b><span style='font-family:  'Calibri','sans-serif';mso-ascii-theme-font:major-latin;mso-hansi-theme-font:  major-latin'>: {$_performancegoal.Comments}<BR/></span><span  style='font-family:'Calibri','sans-serif';mso-ascii-theme-font:major-latin;  mso-hansi-theme-font:major-latin'><o:p></o:p></span></p></td></tr>";

                        if (ds.Tables.Count > 1)
                        {
                            string html1 = "<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 style='border-collapse:collapse;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;mso-border-insideh:none;mso-border-insidev:none'>";

                            //string performanceGoalstd = "<td width=619 valign=top style='width:6.45in;padding:0in 5.4pt 0in 5.4pt'><p class=MsoNormal><b style='mso-bidi-font-weight:normal'><i style='mso-bidi-font-style:normal'><span style='font-family:'Calibri','sans-serif'; mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin'>Goal {$_performancegoal.No}: {$_performancegoal.Name}<o:p></o:p></span></i></b></p> <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><i style='mso-bidi-font-style:normal'><span style='font-family:'Calibri','sans-serif'; mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin'>Achievement Level:</span></i></b><span style='font-family:'Calibri','sans-serif'; mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin'> {$_performancegoal.AchievementLevel}<b style='mso-bidi-font-weight:normal'><i style='mso-bidi-font-style:normal'><o:p></o:p></i></b></span></p> <p class=MsoNormal><span style='font-size:5.0pt;font-family:'Calibri','sans-serif'; mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin'><o:p>&nbsp;</o:p></span></p> <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span  style='font-family:'Calibri','sans-serif';mso-ascii-theme-font:major-latin;  mso-hansi-theme-font:major-latin'>Comments</span></b><span style='font-family:  'Calibri','sans-serif';mso-ascii-theme-font:major-latin;mso-hansi-theme-font:  major-latin'>: {$_performancegoal.Comments}</span><span  style='font-family:'Calibri','sans-serif';mso-ascii-theme-font:major-latin;  mso-hansi-theme-font:major-latin'><o:p></o:p></span></p>  </td>";
                            string updatedDevelopmentGoals = string.Empty;
                            foreach (DataRow drPerformance in ds.Tables[1].Rows)
                            {
                                updatedDevelopmentGoals += performanceGoalstd.Replace("{$_performancegoal.No}", Convert.ToString(drPerformance["GoalNo"])).Replace("{$_performancegoal.Name}", Convert.ToString(drPerformance["GoalDesc"])).Replace("{$_performancegoal.AchievementLevel}", Convert.ToString(drPerformance["AchievementRating"])).Replace("{$_performancegoal.Comments}", Convert.ToString(drPerformance["Comments"]));

                            }
                            if (updatedDevelopmentGoals != string.Empty)
                            {
                                html1 = html1 + updatedDevelopmentGoals + "</table>";
                                strHtml = strHtml.Replace("{$PerformanceGoals}", html1);
                            }
                            else
                            {
                                strHtml = strHtml.Replace("{$PerformanceGoals}", string.Empty);

                            }


                        }
                        if (ds.Tables.Count > 2)
                        {
                            string html1 = "<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 style='border-collapse:collapse;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;mso-border-insideh:none;mso-border-insidev:none'>";

                            //string performanceGoalstd = "<td width=619 valign=top style='width:6.45in;padding:0in 5.4pt 0in 5.4pt'><p class=MsoNormal><b style='mso-bidi-font-weight:normal'><i style='mso-bidi-font-style:normal'><span style='font-family:'Calibri','sans-serif'; mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin'>Goal {$_performancegoal.No}: {$_performancegoal.Name}<o:p></o:p></span></i></b></p> <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><i style='mso-bidi-font-style:normal'><span style='font-family:'Calibri','sans-serif'; mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin'>Achievement Level:</span></i></b><span style='font-family:'Calibri','sans-serif'; mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin'> {$_performancegoal.AchievementLevel}<b style='mso-bidi-font-weight:normal'><i style='mso-bidi-font-style:normal'><o:p></o:p></i></b></span></p> <p class=MsoNormal><span style='font-size:5.0pt;font-family:'Calibri','sans-serif'; mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin'><o:p>&nbsp;</o:p></span></p> <p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span  style='font-family:'Calibri','sans-serif';mso-ascii-theme-font:major-latin;  mso-hansi-theme-font:major-latin'>Comments</span></b><span style='font-family:  'Calibri','sans-serif';mso-ascii-theme-font:major-latin;mso-hansi-theme-font:  major-latin'>: {$_performancegoal.Comments}</span><span  style='font-family:'Calibri','sans-serif';mso-ascii-theme-font:major-latin;  mso-hansi-theme-font:major-latin'><o:p></o:p></span></p>  </td>";
                            string updatedDevelopmentGoals = string.Empty;
                            foreach (DataRow drPerformance in ds.Tables[2].Rows)
                            {
                                updatedDevelopmentGoals += performanceGoalstd.Replace("{$_performancegoal.No}", Convert.ToString(drPerformance["GoalNo"])).Replace("{$_performancegoal.Name}", Convert.ToString(drPerformance["GoalDesc"])).Replace("{$_performancegoal.AchievementLevel}", Convert.ToString(drPerformance["AchievementRating"])).Replace("{$_performancegoal.Comments}", Convert.ToString(drPerformance["Comments"]));

                            }
                            if (updatedDevelopmentGoals != string.Empty)
                            {
                                html1 = html1 + updatedDevelopmentGoals + "</table>";
                                strHtml = strHtml.Replace("{$DevelopmentGoals}", html1);
                            }
                            else
                            {
                                strHtml = strHtml.Replace("{$DevelopmentGoals}", string.Empty);

                            }
                        }

                        string findString = "<img width=211 height=82 src={CompanyLogoPath} align=left hspace=12 v:shapes=Picture_x0020_1>";
                        string image = @"C:\Program Files (x86)\K2 blackpearl\K2 SmartForms Runtime\PlacerCounty\image001.png";
                        //   string replaceValue = "<img width=211 height=82 align=left hspace=12 v:shapes='Picture_x0020_1' name=CompanyLogo src='http://k2.denallix.com//Runtime//PlacerCounty//Image001.png'>";
                        string replaceValue = "<img width=151 height=62 style='top:50px;left:0px;' name=test src='" + image + "'>";
                        strHtml = strHtml.Replace(findString, replaceValue);




                        string Html = string.Empty;
                        try
                        {
                            string newHTMl = updateValueToBookmark(strHtml, dr);
                            Html = newHTMl;
                        }
                        catch (Exception ex)
                        {
                            string str_ex = ex.Message.ToString();
                            System.IO.File.WriteAllText(@"C:\Program Files (x86)\K2 blackpearl\ServiceBroker\logs\file.txt", str_ex);
                        }
                        finally
                        {
                            File.Delete(strDocumentPath);
                            File.Delete(strMapPath + "print.pdf");
                        }
                        //Remove ugly/unnecessary tags so that we don't get junk in the PDF

                        StringReader sr = new StringReader(Html);

                        //create pdf
                        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 50, 50, 50, 50);

                        ////define the Fonts that we will need
                        //iTextSharp.text.Font titleFont = FontFactory.GetFont("Arial", 20, BaseColor.GREEN);
                        //iTextSharp.text.Font titlePageFont = FontFactory.GetFont("Arial", 14, BaseColor.LIGHT_GRAY);
                        //iTextSharp.text.Font link = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.UNDERLINE, new BaseColor(0xB8, 0xB3, 0x08)); //ds 30Apr2013
                        //iTextSharp.text.Font mainfont = FontFactory.GetFont("Arial", 12);
                        //iTextSharp.text.Font mainfontbold = FontFactory.GetFont("Arial-Bold", 12); //ds 30Apr2013
                        //iTextSharp.text.Font highlight = FontFactory.GetFont("Arial", 12, new BaseColor(0xB8, 0xB3, 0x08));
                        //iTextSharp.text.Font TitleFont = FontFactory.GetFont("Arial-Bold", 24, new BaseColor(0xB8, 0xB3, 0x08)); //ds 30Apr2013
                        //iTextSharp.text.Font TitleFontSuper = FontFactory.GetFont("Arial-Bold", 12, new BaseColor(0xB8, 0xB3, 0x08)); //ds 30Apr2013
                        //iTextSharp.text.Font TitleDetail = FontFactory.GetFont("Arial", 12, BaseColor.LIGHT_GRAY); //ds 30Apr2013

                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, new FileStream(strMapPath + pdfPath, FileMode.Create));
                        writer.PageEvent = new ITextEvents();
                        pdfDoc.Open();                        //main content
                        pdfDoc.NewPage();
                        // PdfContentByte cb = writer.DirectContent;

                        //   cb.AddImage(img);
                        StyleSheet styles = new StyleSheet();
                        //styles.LoadTagStyle("a", "color", "#B8B308");
                        //styles.LoadTagStyle("h2", "color", "#B8B308");
                        //styles.LoadTagStyle("h2", "margin-bottom", "0pt");
                        int tableCount = 0;
                        iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph("");
                        paragraph.SpacingBefore = 10;
                        paragraph.SpacingAfter = 10;
                        paragraph.ExtraParagraphSpace = 0;
                        pdfDoc.Add(paragraph);
                        paragraph = new iTextSharp.text.Paragraph("");
                        paragraph.SpacingBefore = 10;
                        paragraph.SpacingAfter = 10;
                        paragraph.ExtraParagraphSpace = 0;
                        //pdfDoc.Add(paragraph);
                        List<IElement> test = HTMLWorker.ParseToList(sr, styles);
                        foreach (IElement el in test)
                        {
                            if (el is iTextSharp.text.Paragraph)
                            {
                                iTextSharp.text.Paragraph p = (iTextSharp.text.Paragraph)el;
                                p.SpacingBefore = 2;
                                p.SpacingAfter = 2;
                                p.ExtraParagraphSpace = 0;
                                var titleFont = FontFactory.GetFont("Calibri (Body)", 12, BaseColor.BLACK);
                                p.Font = titleFont;
                                if (p.Content.Trim() != string.Empty)
                                {
                                    pdfDoc.Add(p);
                                }

                            }
                            else if (el is iTextSharp.text.pdf.PdfPTable)
                            {
                                tableCount++;
                                iTextSharp.text.pdf.PdfPTable p = (iTextSharp.text.pdf.PdfPTable)el;
                                p.WidthPercentage = 90;
                                p.HorizontalAlignment = 0;
                                p.SpacingAfter = 5;
                                p.TotalWidth = 500f;
                                var titleFont = FontFactory.GetFont("Calibri (Body)", 12, BaseColor.BLACK);
                                p.HorizontalAlignment = Element.ALIGN_LEFT;

                                if (tableCount != 2 && tableCount != 3 && tableCount != 4)
                                {
                                    PdfPCell cell = new PdfPCell();
                                    cell.AddElement(p);

                                    cell.BorderWidthBottom = 1f;
                                    cell.BorderWidthLeft = 1f;
                                    cell.BorderWidthTop = 1f;
                                    cell.BorderWidthRight = 1f;
                                    PdfPTable t1 = new PdfPTable(1);
                                    t1.HorizontalAlignment = 0;
                                    t1.SpacingAfter = 5;
                                    t1.TotalWidth = 850f;
                                    t1.WidthPercentage = 100;
                                    t1.HorizontalAlignment = Element.ALIGN_LEFT;
                                    t1.AddCell(cell);

                                    pdfDoc.Add(t1);
                                }
                                else
                                {
                                    pdfDoc.Add(p);
                                }

                            }

                        }

                        pdfDoc.Close();
                        FileStream stream = File.OpenRead(strMapPath + pdfPath);
                        byte[] fileBytes = new byte[stream.Length];
                        //fileBytes=AddPageNumbers(fileBytes);
                        stream.Read(fileBytes, 0, fileBytes.Length);
                        stream.Close();
                        // fsr.Close();

                        PDF = (string)ToFilePropertyValue("File", PDFName + ".pdf", fileBytes);

                        ////Start 15-March-2013
                        //SavePDFToDatabase(strMapPath + "NewFolder1\\" + pdfPath, Convert.ToInt16(dr["Id"]), fname + " " + lname, "MemberStatus");
                        ////End 15-March-2013
                        //string LOBMemberID = dsrate.Tables[0].Rows[0]["LOBID"].ToString(); //ds 30Apr2013 save to memberdirect
                        //string Branch = dsrate.Tables[0].Rows[0]["Branch"].ToString();
                        // SavePDFOnline(strMapPath + pdfPath, "", "");
                        //dsrate.Dispose();
                    }
                    catch (Exception ex)
                    {
                        string str_ex = ex.Message.ToString();
                        System.IO.File.WriteAllText(@"C:\Program Files (x86)\K2 blackpearl\ServiceBroker\logs\file.txt", str_ex);
                    }
                    finally
                    {
                        //File.Delete(strDocumentPath);
                        //File.Delete(strMapPath + "NewFolder1\\" + "print.pdf");
                        //Start 15-March-2013
                        File.Delete(strMapPath + pdfPath);
                        //End 15-March-2013
                    }
                }

            }

            return this;
        }
        public static byte[] AddPageNumbers(byte[] pdf)
        {
            MemoryStream ms = new MemoryStream();
            // we create a reader for a certain document
            PdfReader reader = new PdfReader(pdf);
            // we retrieve the total number of pages
            int n = reader.NumberOfPages;
            // we retrieve the size of the first page
            Rectangle psize = reader.GetPageSize(1);

            // step 1: creation of a document-object
            iTextSharp.text.Document document = new iTextSharp.text.Document(psize, 50, 50, 50, 50);
            // step 2: we create a writer that listens to the document
            PdfWriter writer = PdfWriter.GetInstance(document, ms);
            // step 3: we open the document

            document.Open();
            // step 4: we add content
            PdfContentByte cb = writer.DirectContent;

            int p = 0;
            
            for (int page = 1; page <= reader.NumberOfPages; page++)
            {
                document.NewPage();
                p++;

                PdfImportedPage importedPage = writer.GetImportedPage(reader, page);
                cb.AddTemplate(importedPage, 0, 0);

                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb.BeginText();
                cb.SetFontAndSize(bf, 10);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, +p + "/" + n, 7, 44, 0);
                cb.EndText();
            }
            // step 5: we close the document
            document.Close();
            return ms.ToArray();
        }
        public static iTextSharp.text.Font GetCalibri()
        {
            var fontName = "Calibri";
            if (!FontFactory.IsRegistered(fontName))
            {
                var fontPath = Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\Calibri.ttf";
                FontFactory.Register(fontPath);
            }
            return FontFactory.GetFont(fontName, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        }

        public static object ToFilePropertyValue(string propName, string filename, byte[] content)
        {
            return (new FileProperty(propName, new SourceCode.SmartObjects.Services.ServiceSDK.Objects.MetaData(), filename, Convert.ToBase64String(content))).Value;
        }
        private void SavePDFOnline(string PDF, string memberid, string Branch)
        {
            string mmbrid = "1111";
            string StatementPath = @"C:\SmartObject_PDF";
            if (!Directory.Exists(StatementPath + mmbrid))
            {
                Directory.CreateDirectory(StatementPath + mmbrid);
            }
            string PDFDest = StatementPath + mmbrid + @"\" + "ISFS" + mmbrid + DateTime.Now.ToString("ddMMyyyy") + ".pdf";
            File.Copy(PDF, PDFDest, true);
        }
        private string updateValueToBookmark(string strHtml, DataRow dr)
        {
            string newHTML = string.Empty;//.em
            foreach (DataColumn dc in dr.Table.Columns)
            {

                string findString = "{$" + dc.ColumnName + "}";
                string replaceValue = Convert.ToString(dr[dc.ColumnName]);
                strHtml = strHtml.Replace(findString, replaceValue);
            }
            newHTML = strHtml;
            return newHTML;
        }
        private string getDocumentTemplate(string templateName, string strMapPath)
        {
            Guid newGuid = Guid.NewGuid();

            string tempDocument = String.Format("{0}{1}.{2}", strMapPath, newGuid.ToString(), "docx");

            File.Copy(templateName, tempDocument);

            return tempDocument;
        }
        public DataSet GetDocumentData(int employee_id)
        {
            string storedProcedureName = string.Empty;
            if (!string.IsNullOrEmpty((string)ServiceConfiguration["StoredProcedureName"]))
            {
                storedProcedureName = (string)ServiceConfiguration["StoredProcedureName"];
            }
            DataSet ds = new DataSet();
            if (!string.IsNullOrEmpty(storedProcedureName))
            {
                sqlConnection = new SqlConnection(GetConnectionString());
                sqlConnection.Open();
                SqlCommand sqlCommand = GetCommand(sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = storedProcedureName;
                sqlCommand.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = employee_id;
                sqlCommand.ExecuteNonQuery();
                SqlDataAdapter ad = new SqlDataAdapter(sqlCommand);
                ad.Fill(ds);
                sqlConnection.Close();
            }
            return ds;
        }

        public SqlCommand GetCommand(SqlConnection objSqlConnection)
        {
            SqlCommand objCommand = objSqlConnection.CreateCommand();
            return objCommand;
        }

        public string GetConnectionString()
        {
            //string connectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);
            string connectionString = string.Empty;
            if (!string.IsNullOrEmpty((string)ServiceConfiguration["DBConnectionString"]))
            {
                connectionString = (string)ServiceConfiguration["DBConnectionString"];
            }
            return connectionString;
        }

        SqlConnection sqlConnection = null;

        public SqlConnection GetSQLDataConnection()
        {
            sqlConnection = new SqlConnection(GetConnectionString());
            try
            {
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                    sqlConnection.Open();
            }
            catch (Exception ex)
            {

            }

            return sqlConnection;
        }

        private DataSet GetImagePaths()
        {
            DataSet ds = new DataSet();

            SqlCommand sqlCommand = GetCommand(GetSQLDataConnection());
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "GetImagePaths";
            sqlCommand.ExecuteNonQuery();
            SqlDataAdapter ad = new SqlDataAdapter(sqlCommand);
            ad.Fill(ds);

            return ds;
        }
    }

    public class ITextEvents : PdfPageEventHelper
    {

        // This is the contentbyte object of the writer
        PdfContentByte cb;

        // we will put the final number of pages in a template
        PdfTemplate headerTemplate, footerTemplate;

        // this is the BaseFont we are going to use for the header / footer
        BaseFont bf = null;

        // This keeps track of the creation time
        DateTime PrintTime = DateTime.Now;


        #region Fields
        private string _header;
        #endregion

        #region Properties
        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }
        #endregion


        public override void OnOpenDocument(PdfWriter writer, iTextSharp.text.Document document)
        {
            try
            {
                PrintTime = DateTime.Now;
                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.DirectContent;
                headerTemplate = cb.CreateTemplate(100, 100);
                footerTemplate = cb.CreateTemplate(50, 50);
            }
            catch (DocumentException de)
            {

            }
            catch (System.IO.IOException ioe)
            {

            }
        }

        public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        {
            base.OnEndPage(writer, document);

            iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);

            iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

            Phrase p1Header = new Phrase("Sample Header Here", baseFontNormal);

            //Create PdfTable object
            PdfPTable pdfTab = new PdfPTable(3);

            //We will have to create separate cells to include image logo and 2 separate strings
            //Row 1
            PdfPCell pdfCell1 = new PdfPCell();
            PdfPCell pdfCell2 = new PdfPCell(p1Header);
            PdfPCell pdfCell3 = new PdfPCell();
            String text = "Page " + writer.PageNumber + " of ";


            //Add paging to header
            /*
            {
                cb.BeginText();
                cb.SetFontAndSize(bf, 12);
                cb.SetTextMatrix(document.PageSize.GetRight(200), document.PageSize.GetTop(45));
                cb.ShowText(text);
                cb.EndText();
                float len = bf.GetWidthPoint(text, 12);
                //Adds "12" in Page 1 of 12
                cb.AddTemplate(headerTemplate, document.PageSize.GetRight(200) + len, document.PageSize.GetTop(45));
            }*/
            //Add paging to footer
            {
                cb.BeginText();
                cb.SetFontAndSize(bf, 12);
                cb.SetTextMatrix(document.PageSize.GetRight(180), document.PageSize.GetBottom(30));
                cb.ShowText(text);
                cb.EndText();
                float len = bf.GetWidthPoint(text, 12);
                cb.AddTemplate(footerTemplate, document.PageSize.GetRight(180) + len, document.PageSize.GetBottom(30));
            }
            //Row 2
            PdfPCell pdfCell4 = new PdfPCell(new Phrase("Sub Header Description", baseFontNormal));
            //Row 3


            PdfPCell pdfCell5 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
            PdfPCell pdfCell6 = new PdfPCell();
            PdfPCell pdfCell7 = new PdfPCell(new Phrase("TIME:" + string.Format("{0:t}", DateTime.Now), baseFontBig));


            //set the alignment of all three cells and set border to 0
            pdfCell1.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell3.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell4.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell5.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell6.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell7.HorizontalAlignment = Element.ALIGN_CENTER;


            pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;
            pdfCell3.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfCell4.VerticalAlignment = Element.ALIGN_TOP;
            pdfCell5.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfCell6.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfCell7.VerticalAlignment = Element.ALIGN_MIDDLE;


            pdfCell4.Colspan = 3;



            pdfCell1.Border = 0;
            pdfCell2.Border = 0;
            pdfCell3.Border = 0;
            pdfCell4.Border = 0;
            pdfCell5.Border = 0;
            pdfCell6.Border = 0;
            pdfCell7.Border = 0;


            //add all three cells into PdfTable
            /*
            pdfTab.AddCell(pdfCell1);
            pdfTab.AddCell(pdfCell2);
            pdfTab.AddCell(pdfCell3);
            pdfTab.AddCell(pdfCell4);
            pdfTab.AddCell(pdfCell5);
            pdfTab.AddCell(pdfCell6);
            pdfTab.AddCell(pdfCell7);
            */
            pdfTab.TotalWidth = document.PageSize.Width - 80f;
            pdfTab.WidthPercentage = 70;
            //pdfTab.HorizontalAlignment = Element.ALIGN_CENTER;


            //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
            //first param is start row. -1 indicates there is no end row and all the rows to be included to write
            //Third and fourth param is x and y position to start writing
            pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);
            //set pdfContent value

            //Move the pointer and draw line to separate header section from rest of page
            //cb.MoveTo(40, document.PageSize.Height - 100);
            //cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
            //cb.Stroke();

            //Move the pointer and draw line to separate footer section from rest of page
            cb.MoveTo(40, document.PageSize.GetBottom(50));
            cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50));
            cb.Stroke();
        }

        public override void OnCloseDocument(PdfWriter writer, iTextSharp.text.Document document)
        {
            base.OnCloseDocument(writer, document);

            headerTemplate.BeginText();
            headerTemplate.SetFontAndSize(bf, 12);
            headerTemplate.SetTextMatrix(0, 0);
            headerTemplate.ShowText((writer.PageNumber - 1).ToString());
            headerTemplate.EndText();

            footerTemplate.BeginText();
            footerTemplate.SetFontAndSize(bf, 12);
            footerTemplate.SetTextMatrix(0, 0);
            footerTemplate.ShowText((writer.PageNumber - 1).ToString());
            footerTemplate.EndText();


        }
    }
}
