﻿using System;
using System.Collections.Generic;
using System.Text;
using SourceCode.SmartObjects.Services.ServiceSDK;
namespace PlacerCounty.DocMerge
{
    public class ServiceBroker : ServiceAssemblyBase
    {
        #region DescribeSchema
        //Registers any service objects in this assembly
        public override string DescribeSchema()
        {

            Type[] types = this.GetType().Assembly.GetTypes();

            foreach (Type t in types)
            {
                if (t.IsClass)
                {
                    if (t.GetCustomAttributes(typeof(SourceCode.SmartObjects.Services.ServiceSDK.Attributes.ServiceObjectAttribute), false).Length > 0)
                    {
                        base.Service.ServiceObjects.Add(new SourceCode.SmartObjects.Services.ServiceSDK.Objects.ServiceObject(t));
                    }
                }
            }


            return base.DescribeSchema();
        }
        #endregion

        //override the GetConfigSection method to add any config setting variables that
        //you would like to be set in the admin tool or to set impersonation information
        public override string GetConfigSection()
        {
            //Provide config setting options that you would like set in the admin tool
            //These settings can be anything that might change from environment to 
            //environment, such as a Web Service URL or database server name

            //give the setting a name, whether it's required or not and a default value(optional)
            Service.ServiceConfiguration.Add("DBConnectionString", true, "");
            Service.ServiceConfiguration.Add("HTMLDocumentPath", true, "");
            Service.ServiceConfiguration.Add("ComplanyLogoPath", true, "");
            Service.ServiceConfiguration.Add("StoredProcedureName", true, "");
            Service.ServiceConfiguration.Add("PDFName", true, "");

            //use this setting if you want to impersonat the logged on user when calling
            //the backend system
            //Service.ServiceConfiguration.ServiceAuthentication.Impersonate = true;
            //string connection = Service.ServiceConfiguration["DBConnection"].ToString();  
            return base.GetConfigSection();
        }

        #region Extend
        //override the Extend method to include additional exception handling as needed
        public override void Extend()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

        #region Execute
        //The Execute method is used to call the appropriate ServiceObject method
        //and can be overriden to provide additional logic for executing the
        //ServiceObject methods
        public override void Execute()
        {
            base.Execute();
        }
        #endregion
    }
}
